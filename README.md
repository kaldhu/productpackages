# ProductPackages
Created by: Phillip Smith
Email: kaldhu@yahoo.com

This site is currently being hosted at https://productpackages.azurewebsites.net.
MongoDB database is hosted at kaldhu:w9YPHd00gE@productpackages-ytd7e.azure.mongodb.net/PackagesAPI

uses 3rd party API "product-service" to get Product information, this is hosted at https://product-service.herokuapp.com/api/v1/products
uses 3rd party API "Exchange Rates API" to get Exchange rate information, this is hosted at https://api.exchangeratesapi.io/

The Api is accessed via 

```
    /api/packages
        > get
        > post

    /apo/packages/:packageId
        > get
        > put
        > patch
        > delete
```

To host this locallaly, you will need MongoDB installed on to your machine and change the db values within app.js (line 21 & 24).

When hosted locally the system will still need internet access to connect to remote servers responsible for Products and exchange rate calculations.

If hosting locally after modifiying the db value, open the command window

> npm install 
> npm start

The site should be then hosted at http://localhost:3000/

Please excuse the project name, I was trying to get something to deploy to azure and this is the only project that managed it.
