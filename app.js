﻿'use strict';
const debug = require('debug');
const express = require('express');
const path = require('path');
const logger = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const app = express();

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'public')));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

//DEVNOTE: Uncomment this if you want to use a local database
//const db = mongoose.connect('mongodb://localhost/packageAPI');

//DEVNOTE: Comment this if you want to use a local database
const db = mongoose.connect('mongodb+srv://kaldhu:w9YPHd00gE@productpackages-ytd7e.azure.mongodb.net/PackagesAPI?retryWrites=true&w=majority');

const ProjectPackage = require('./models/projectPackageModel');



// #region Routes

const packageRouter = require('./routes/productPackageRouter')(ProjectPackage);
const routes = require('./routes/index');

app.use('/', routes);
app.use('/api', packageRouter);

// #endregion

// #region Error Handling

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}
// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

// #endregion

// #region Server Setup

app.set('port', process.env.PORT || 3000);

var server = app.listen(app.get('port'), function () {
    debug('Express server listening on port ' + server.address().port);
});

// #endregion
