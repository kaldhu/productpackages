﻿db.packages.insert([
    {
        Name: "Empty Package",
        Description: "has no products",
        Products: [
        ]
    },
    {
        Name: "Single Package",
        Description: "has one product",

        Products: [
            {
                Id: "DXSQpv6XVeJm"
            }
         ]
    },
    {
        Name: "Full Package",
        Description: "has all products",
        Products: [
            {
                Id: "DXSQpv6XVeJm"
            },
            {
                Id: "7dgX6XzU3Wds"
            },
            {
                Id: "PKM5pGAh9yGm"
            },
            {
                Id: "7Hv0hA2nmci7"
            },
            {
                Id: "500R5EHvNlNB"
            },
            {
                Id: "IP3cv7TcZhQn"
            },
            {
                Id: "IJOHGYkY2CYq"
            },
            {
                Id: "8anPsR2jbfNW"
            },
        ]
    }
])
