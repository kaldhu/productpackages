﻿const mongoose = require('mongoose');

const { Schema } = mongoose;

const projectPackage = new Schema(
    {
        Name: { type: String },
        Description: { type: String },
        Products: [{ Id: { type: String } }],
    }
);

module.exports = mongoose.model('projectPackages', projectPackage);