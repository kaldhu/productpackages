﻿
function productPackageController(ProjectPackage, productPackageManager) {

    function post(request, result) {
        const projectPackage = new ProjectPackage(request.body);
        projectPackage.save();
        return result.status(201).json(projectPackage);
    }

    async function get(request, result) {
        ProjectPackage.find(async function (err, productPackages) {
            if (err) {
                return result.send(err);
            }

            let jsonArray = [];
            for (let index in productPackages) {
                let productPackage = productPackages[index];
                try {

                    const dataTransferObject = await getProductPackageDataHelper(productPackage, request);
                    jsonArray.push(dataTransferObject);

                } catch (error) {
                    console.log("Error Occurred getting product data: " + err);
                    return result.status(500).send(error);
                }
            }

            return result.json(jsonArray);
        });
    }

    async function getById(request, result) {
        const { productPackage } = request;
        try {
            const dataTransferObject = await getProductPackageDataHelper(productPackage, request);
            return result.json(dataTransferObject);
        } catch (error) {
            console.log("Error Occurred getting product data: " + err);
            return result.status(500).send(error);
        }
    }

    function put(request, result) {
        const { productPackage } = request;
        productPackage.Name = request.body.Name;
        productPackage.Description = request.body.Description;
        productPackage.Products = request.body.Products;
        productPackage.save((err) => {
            if (err) {
                return result.send(err);
            }
            return result.json(productPackage);
        });
    }

    function remove (request, result){
        request.productPackage.remove((err) => {
            if (err) {
                return result.send(err);
            }
            return result.sendStatus(204);
        });
    }

    function patch(request, result) {
        const { productPackage } = request;
        //DEVNOTE: Ensure we do not override the Id
        if (request.body._id) {
            delete request.body._id;
        }
        Object.entries(request.body).forEach((item) => {
            productPackage[item[0]] = item[1];
        });
        productPackage.save((err) => {
            if (err) {
                return result.send(err);
            }
            return result.json(productPackage);
        });
    }

    // #region Helper Functions

    async function getProductPackageDataHelper(productPackage, request) {
        const { query } = request;

        const dataTransferObject = productPackageManager.getProductPackageDataTransferObject(productPackage, request.headers.host);

        const amount = await productPackageManager.getTotalProductPackagePrice(dataTransferObject.Products);
        const conversionResult = await productPackageManager.getConvertedProductPackagePrice(amount, query['currency']);

        dataTransferObject.Currency = conversionResult["Currency"];
        dataTransferObject.TotalValue = conversionResult["Amount"];
        return dataTransferObject;
    }

    // #endregion

    return { post, get, getById, put, patch, remove };
}

module.exports = productPackageController;