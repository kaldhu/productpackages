﻿const express = require('express');

const productPackageManager = require('../managers/productPackageManager')();
const productPackage = express.Router();
const productPackageController = require('../controller/productPackageController');

function routes(ProjectPackage) {

    // #region /packages

    const controller = productPackageController(ProjectPackage, productPackageManager);

    productPackage.route('/packages')
        .get(controller.get)
        .post(controller.post);

    // #endregion

    // #region /packages/:packageId

    productPackage.use('/packages/:packageId', (request, result, next) => {
        ProjectPackage.findById(request.params.packageId, (err, productPackage) => {
            if (err) {
                return result.send(err);
            }
            if (productPackage) {
                request.productPackage = productPackage;
                return next();
            }
            return result.sendStatus(404);
        });
    });

    productPackage.route('/packages/:packageId')
        .get(controller.getById)
        .put(controller.put)
        .patch(controller.patch)
        .delete(controller.remove);

    // #endregion

    return productPackage;
}

module.exports = routes;