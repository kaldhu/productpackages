﻿'use strict';
var express = require('express');

var router = express.Router();

const productManager = require('../managers/productManager')();
const currencyManager = require('../managers/currencyManager')(true);



/* GET home page. */
router.get('/', async function (req, res) {
    let result = "error";
    let isValid = false;
    let amount = 0;
    try {
        result = await productManager.getProductPrice('DXSQpv6XVeJm');
        isValid = await currencyManager.checkIfValidCurrency('CAD');
        amount = await currencyManager.convertAmountToCurrency(300, 'CAD');
    } catch (err) {
        result = "Unable to find Product - " + err;
    }
    res.render('index', { title: result });
});


module.exports = router;
