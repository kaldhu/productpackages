﻿const fetch = require('node-fetch');

function currencyManager(enableCaching) {
    
    async function checkIfValidCurrency(currency) {
        const currencyAPI = process.env.currencyAPI
        const response = await fetch(
            `${currencyAPI}/api/currency/checkCurrency?currency=${currency}`);
        return await response.json();
    }

    async function convertAmountToCurrency(amount, currency) {
        const currencyAPI = process.env.currencyAPI
        const response = await fetch(
            `${currencyAPI}/api/currency/exchangeCurrency?amount=${amount}&currency=${currency}`);
        return await response.json();
    }

    return { checkIfValidCurrency, convertAmountToCurrency };
}

module.exports = currencyManager;