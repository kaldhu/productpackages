﻿
const productManager = require('./productManager')();
const currencyManager = require('./currencyManager')(true);

function productPackageManager() {

    const defaultCurrency = process.env.defaultCurrency;

    function getProductPackageDataTransferObject(productPackage, host = 'http://localhost:1337') {
        const productApi = process.env.productAPI
        const dataTransferObject = productPackage.toJSON();
        dataTransferObject.Products.map((product) => {
            const newProduct = {
                Id: product.Id
            };
            product.links = {};
            product.links.self = `${productApi}/api/product/${newProduct.Id}`;
            return newProduct;
        });

        dataTransferObject.links = {};
        dataTransferObject.links.self = `http://${host}/api/packages/${productPackage._id}`;
        return dataTransferObject;
    }

    async function getTotalProductPackagePrice(products) {
        let totalValue = 0;
        for (let productIndex in products) {
            let productId = products[productIndex].Id;

            try {
                let productValue = await productManager.getProductPrice(productId);
                totalValue = totalValue + productValue;
            } catch (err) {
                console.log("Error Occurred getting product price: " + err);
                //DEVNOTE: Continuing as if price is 0 for failed product
            }
        }
        return totalValue;
    }

    async function getConvertedProductPackagePrice(amount, currency = defaultCurrency) {
        try {
            if (currency !== defaultCurrency) {
                const isValid = await currencyManager.checkIfValidCurrency(currency);
                if (isValid) {
                    amount = await currencyManager.convertAmountToCurrency(amount, currency);
                }
                else {
                    //DEVNOTE: Unable to find requested currency defaulting to USD
                    currency = defaultCurrency;
                }
            }
        } catch (err) {
            console.log("Error Occurred getting converting to another currency: " + err);
            currency = defaultCurrency;
        }

        return {
            Currency: currency,
            Amount: amount
        };
    }
    return { getProductPackageDataTransferObject, getTotalProductPackagePrice, getConvertedProductPackagePrice };
}

module.exports = productPackageManager;