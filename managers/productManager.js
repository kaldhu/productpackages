﻿const fetch = require('node-fetch');

function productManager() {

    async function getProductPrice(id) {
        const productApi = process.env.productAPI
        const response = await fetch(`${productApi}/api/product/${id}/get_price`,
            {
                method: 'get'
            });
        const json = await response.json();
        return json
    }

    return { getProductPrice };
}

module.exports = productManager;